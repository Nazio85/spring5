package ru.xway.conf;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.Properties;

@EnableTransactionManagement
@PropertySource("classpath:db-conf.properties")
public class DataSourceConfiguration {

    /**
     * Создания источника данных
     * Задание параметров подключения к базе данны
     *
     * @return
     */
    @Bean("dataSource")
    public DataSource dataSource(@Value("${datasource.driver}") String dataSourceDriver,
                                 @Value("${datasource.url}") String dataSourceUrl,
                                 @Value("${datasource.user}") String dataSourceUser,
                                 @Value("${datasource.password}") String dataSourcePassword) {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(dataSourceDriver);
        dataSource.setUrl(dataSourceUrl);
        dataSource.setUsername(dataSourceUser);
//        dataSource.setPassword(dataSourcePassword);
        return dataSource;
    }


    /**
     * Создание класса фабрики, реализующей интерфейс FactoryBean<EntityManagerFactory>
     *
     * @return
     */
    @Bean("entityManagerFactory")
    public LocalContainerEntityManagerFactoryBean
    entityManagerFactory(@Qualifier("dataSource") DataSource dataSource,
                         @Value("${hibernate.max_fetch_depth}") int maxFetchDepth,
                         @Value("${hibernate.jdbc.fetch_size}") int fetchSize,
                         @Value("${hibernate.jdbc.batch_size}") int batchSize,
                         @Value("${hibernate.show_sql}") boolean showSql,
                         @Value("${hibernate.hbm2ddl.auto}") String tableStrategy) {
        LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();
        factoryBean.setDataSource(dataSource);// Задание источника подключения
        //Задание адаптера для конкретной реализации JPA указывает, какая именно библиотека будет использоваться в качестве поставщика постоянства
        factoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());

        factoryBean.setPackagesToScan("ru.xway.model");

        Properties properties = new Properties();// Создание свойств для настройки Hibernate
        properties.put("hibernate.max_fetch_depth", maxFetchDepth);// Указание максимальной глубины связи (будет рассмотрено в следующем уроке)
        properties.put("hibernate.jdbc.fetch_size", fetchSize);// Определение максимального количества строк, возвращаемых за один запрос из БД
        properties.put("hibernate.jdbc.batch_size", batchSize);// Определение максимального количества запросов при использовании пакетных операций
        properties.put("hibernate.show_sql", showSql);// Включает логирование
        properties.put("hibernate.hbm2ddl.auto", tableStrategy);//Автоматическое создаие таблиц

        factoryBean.setJpaProperties(properties);
        return factoryBean;
    }

    @Bean("transactionManager")
    public JpaTransactionManager transactionManager(@Qualifier("entityManagerFactory") LocalContainerEntityManagerFactoryBean entityManagerFactory) {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory.getObject());
        return transactionManager;
    }

}

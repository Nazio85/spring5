package ru.xway.conf;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import ru.xway.model.Animal;
import ru.xway.model.Cat;
import ru.xway.model.SimpleCalculator;

@Configuration
@ComponentScan("ru.xway")
@Import(DataSourceConfiguration.class)
@EnableJpaRepositories("ru.xway.dao")
public class MainConfig {

    @Bean(name = "Barsik")
    public Animal createBarsik(@Value("Барсик") String name) {
        return new Cat(name, 5);
    }

    @Bean
    public SimpleCalculator calculator() {
        return new SimpleCalculator();
    }


}


//package ru.xway.dao.criteria;
//
//import com.ecommpay.host.hap.dao.dblocal.entities.InCards_;
//import com.ecommpay.host.hap.dao.dblocal.entities.account.AccountForSearch_;
//import com.ecommpay.host.hap.dao.dblocal.entities.card.CardForSearch;
//import com.ecommpay.host.hap.dao.dblocal.entities.card.CardForSearch_;
//import com.ecommpay.host.hap.dao.dblocal.entities.card.CardholderForSearch;
//import com.ecommpay.host.hap.dao.dblocal.entities.card.CardholderForSearch_;
//import com.ecommpay.host.hap.dao.dblocal.entities.phone.PhoneForSearch_;
//import com.ecommpay.host.hap.dao.dblocal.entities.phone.PhonemapForSearch_;
//import com.ecommpay.host.hap.dao.dblocal.repositories.card.CardSearchRepository;
//import com.ecommpay.host.hap.dto.RequestSearchCardholderDto;
//import org.springframework.data.domain.Page;
//import org.springframework.data.domain.Pageable;
//import org.springframework.data.jpa.domain.Specification;
//import org.springframework.stereotype.Repository;
//
//import javax.persistence.criteria.*;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Objects;
//
//
//@Repository
//public class CardholderCriteriaRepository {
//
//    /**
//     * CardSearchRepository Должен быть наследником JpaSpecificationExecutor<CardholderForSearch>
//     */
//    private final CardSearchRepository cardSearchRepository;
//
//    public CardholderCriteriaRepository(CardSearchRepository searchRepository) {
//        this.cardSearchRepository = searchRepository;
//    }
//
//    public Page<CardholderForSearch> findAllForSearchPage(RequestSearchCardholderDto dto, int agentId, Pageable pageable) {
//        Specification<CardholderForSearch> cardholderSpecification =
//                (root, criteriaQuery, criteriaBuilder) -> {
//                    List<Predicate> predicateList = new ArrayList<>();
//
//                    if (Objects.nonNull(dto.getLastName()) && !dto.getLastName().trim().isEmpty()) {
//                        Predicate lastname = findForName(dto.getLastName(), "lastname", agentId, criteriaBuilder, root);
//                        predicateList.add(lastname);
//                    }
//
//                    if (Objects.nonNull(dto.getFirstName()) && !dto.getFirstName().trim().isEmpty()) {
//                        Predicate firstname = findForName(dto.getFirstName(), "firstname", agentId, criteriaBuilder, root);
//                        predicateList.add(firstname);
//                    }
//
//                    if (Objects.nonNull(dto.getOtherNames()) && !dto.getOtherNames().trim().isEmpty()) {
//                        Predicate othernames = findForName(dto.getOtherNames(), "othernames", agentId, criteriaBuilder, root);
//                        predicateList.add(othernames);
//                    }
//
//                    if (Objects.nonNull(dto.getAppId()) && !dto.getAppId().trim().isEmpty()) {
//                        Predicate forAppId = findForAppId(dto.getAppId(), agentId, criteriaBuilder, root);
//                        predicateList.add(forAppId);
//                    }
//
//                    if (Objects.nonNull(dto.getPhone()) && !dto.getPhone().trim().isEmpty()) {
//                        Predicate forPhone = findForPhone(dto.getPhone(), agentId, criteriaBuilder, root);
//                        predicateList.add(forPhone);
//                    }
//
//                    if (Objects.nonNull(dto.getFullPan()) && dto.getFullPan().length() == 16) {
//                        Predicate forFullPan = findForFullPan(dto, agentId, criteriaBuilder, root);
//                        predicateList.add(forFullPan);
//                    } else if (Objects.nonNull(dto.getPanFirstSix()) && dto.getPanFirstSix().length() == 6
//                            && Objects.nonNull(dto.getPanLastFour()) && dto.getPanLastFour().length() == 4) {
//                            Predicate forPartPan = findForPartPan(dto.getPanFirstSix(),
//                                    dto.getPanLastFour(), agentId, criteriaBuilder, root);
//                            predicateList.add(forPartPan);
//                    }
//
//                    if (predicateList.size() == 0){
//                        return criteriaBuilder.isTrue(criteriaBuilder.literal(false));
//                    }
//                    Predicate[] predicates = predicateList.toArray(new Predicate[0]);
//                    return criteriaBuilder.and(predicates);
//                };
//
//
//        return cardSearchRepository.findAll(cardholderSpecification, pageable);
//    }
//
//
//    /**
//     * Поиск по по номеру
//     */
//    private Predicate findForPhone(String phone, int agentId, CriteriaBuilder criteriaBuilder,
//                                   Root<CardholderForSearch> root) {
//        Path<String> phonePath = root
//                .join(CardholderForSearch_.accounts, JoinType.INNER)
//                .join(AccountForSearch_.card, JoinType.INNER)
//                .join(CardForSearch_.phonemaps, JoinType.INNER)
//                .join(PhonemapForSearch_.phone, JoinType.INNER)
//                .get(PhoneForSearch_.PHONENO);
//
//        Expression<String> expressionSearch = criteriaBuilder.literal("%" + phone + "%");
//        Predicate predicate = criteriaBuilder.like(phonePath, expressionSearch);
//
//        if (agentId == 0) {
//            return predicate;
//        } else {
//            Predicate predicateForAgent = getCurrentAgentPredicate(agentId, criteriaBuilder, root);
//            return criteriaBuilder.and(predicate, predicateForAgent);
//        }
//    }
//
//
//    /**
//     * Выборка из нескольких вложенностей коллекции
//     * В первую очередь объединяем таблицы, далее из таблицы InCards получаем appid
//     * Сравниваем appId с appId из запроса
//     * Если user agent не 0 тогда добавляем к запросу id текущего агента и вытаскиваем только для него
//     */
//    private Predicate findForAppId(String appId, int agentId, CriteriaBuilder criteriaBuilder,
//                                   Root<CardholderForSearch> root) {
//        Path<String> appIdPath = root
//                .join(CardholderForSearch_.accounts, JoinType.INNER)
//                .join(AccountForSearch_.card, JoinType.INNER)
//                .join(CardForSearch_.inCards)
//                .get(InCards_.appid);
//
//        Predicate predicate = criteriaBuilder.equal(appIdPath, criteriaBuilder.literal(appId));
//
//        if (agentId == 0) {
//            return predicate;
//        } else {
//            Predicate predicateForAgent = getCurrentAgentPredicate(agentId, criteriaBuilder, root);
//            return criteriaBuilder.and(predicate, predicateForAgent);
//        }
//    }
//
//
//    /**
//     * Условие выбор по фамилии
//     * Если первые буквы фамилий  базе данных совпадают с dto.lastName и ID == 0 (super user)
//     * тогда вытаскиваем всех кто совпадает,
//     * в противном случае всех кто совпадает, но только для текущего агента.
//     */
//    private Predicate findForName(String name, String fieldName, int agentId,
//                                  CriteriaBuilder criteriaBuilder, Root<CardholderForSearch> root) {
//        Expression<String> expressionSearch = criteriaBuilder.literal(name.toLowerCase() + "%");
//        Expression<String> expressionBd = root.get(fieldName).as(String.class);
//
//        Predicate like = criteriaBuilder.like(criteriaBuilder.lower(expressionBd), expressionSearch);
//
//        if (agentId == 0) {
//            return like;
//        } else {
//            Predicate predicate = getCurrentAgentPredicate(agentId, criteriaBuilder, root);
//            return criteriaBuilder.and(like, predicate);
//        }
//    }
//
//
//    /**
//     * Запрос для сверки id агента
//     */
//    private Predicate getCurrentAgentPredicate(int agentId, CriteriaBuilder criteriaBuilder, Root<CardholderForSearch> root) {
//        Path<Integer> currentAgentId = root
//                .get("inPerson")
//                .get("inLoads")
//                .get("agent")
//                .get("aid");
//
//        return criteriaBuilder.equal(currentAgentId, criteriaBuilder.literal(agentId));
//    }
//
//    /**
//     * Условие выбор по полному PAN
//     */
//    private Predicate findForFullPan(RequestSearchCardholderDto dto, int agentId, CriteriaBuilder criteriaBuilder,
//                                     Root<CardholderForSearch> root) {
//
//        String fullPan = dto.getFullPan();
//
//        Path<CardForSearch> appIdPath = root
//                .join(CardholderForSearch_.accounts, JoinType.INNER)
//                .join(AccountForSearch_.card, JoinType.INNER);
//
//        Path<String> cardBinPath = appIdPath.get(CardForSearch_.cardbin);
//        Path<String> cardBodyPath = appIdPath.get(CardForSearch_.cardbody);
//        Path<String> cardCheckPath = appIdPath.get(CardForSearch_.cardcheck);
//
//        Predicate predicateCardBin = criteriaBuilder.equal(cardBinPath, criteriaBuilder.literal(fullPan.substring(0, 6)));
//        Predicate predicateBodyPath = criteriaBuilder.equal(cardBodyPath, criteriaBuilder.literal(fullPan.substring(6, 15)));
//        Predicate predicateCheckPath = criteriaBuilder.equal(cardCheckPath, criteriaBuilder.literal(fullPan.substring(15, 16)));
//
//        if (agentId == 0) {
//            return criteriaBuilder.and(predicateCardBin, predicateBodyPath, predicateCheckPath);
//        } else {
//            Predicate predicate = getCurrentAgentPredicate(agentId, criteriaBuilder, root);
//            return criteriaBuilder.and(predicateCardBin, predicateBodyPath, predicateCheckPath, predicate);
//        }
//    }
//
//    /**
//     * Условие выбор по полному PAN
//     */
//    private Predicate findForPartPan(String panFirstSix, String panLastFour, int agentId, CriteriaBuilder criteriaBuilder,
//                                     Root<CardholderForSearch> root) {
//        Path<CardForSearch> appIdPath = root
//                .join(CardholderForSearch_.accounts, JoinType.INNER)
//                .join(AccountForSearch_.card, JoinType.INNER);
//
//        Path<String> cardBodyPath = appIdPath.get(CardForSearch_.cardbody);
//        Path<String> cardBinPath = appIdPath.get(CardForSearch_.cardbin);
//        Path<String> cardCheckPath = appIdPath.get(CardForSearch_.cardcheck);
//
//        Predicate predicatePanFirstSix = criteriaBuilder.equal(cardBinPath, criteriaBuilder.literal(panFirstSix));
//        Predicate predicateBodyPath = criteriaBuilder.like(cardBodyPath, criteriaBuilder.literal(
//                "%" + panLastFour.substring(0, 3)));
//        Predicate predicatePanLastFour = criteriaBuilder.equal(cardCheckPath, criteriaBuilder.literal(panLastFour.substring(3,4)));
//
//        if (agentId == 0) {
//            return criteriaBuilder.and(predicatePanFirstSix, predicateBodyPath, predicatePanLastFour);
//        } else {
//            Predicate predicate = getCurrentAgentPredicate(agentId, criteriaBuilder, root);
//            return criteriaBuilder.and(predicatePanFirstSix, predicatePanLastFour, predicate);
//        }
//    }
//}

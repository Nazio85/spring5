package ru.xway.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.xway.model.Employee;

import java.util.Optional;

@Repository("employeeRepositoryJpa")
public interface EmployeeRepositoryJpa extends JpaRepository<Employee, Long> {
    Optional<Employee> findById(Long id);
}

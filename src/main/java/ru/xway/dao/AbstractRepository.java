package ru.xway.dao;

import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Repository
public class AbstractRepository {

    @PersistenceContext
    protected EntityManager entityManager;

}

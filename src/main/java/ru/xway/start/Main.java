package ru.xway.start;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.xway.conf.MainConfig;
import ru.xway.dao.DepartmentRepository;
import ru.xway.dao.EmployeeRepository;
import ru.xway.dao.UserRepository;
import ru.xway.model.Animal;
import ru.xway.model.Departments;
import ru.xway.model.Drink;
import ru.xway.model.User;

import java.util.List;


public class Main {


    public static void main(String[] args) throws Exception {
        AnnotationConfigApplicationContext annotationConfigApplicationContext =
                new AnnotationConfigApplicationContext(MainConfig.class);

//
        EmployeeRepository employeeRepository = annotationConfigApplicationContext.getBean("employeeRepository", EmployeeRepository.class);
//        EmployeeRepositoryJpa employeeRepositoryJpa = annotationConfigApplicationContext.getBean("employeeRepositoryJpa", EmployeeRepositoryJpa.class);
        DepartmentRepository departmentRepository = annotationConfigApplicationContext.getBean("departments", DepartmentRepository.class);

        Drink bean = annotationConfigApplicationContext.getBean(Drink.class);
        bean.getDrink();

//        List<Departments> findAll = departmentRepository.findAll;
//        for (Departments departments : findAll) {
//            System.out.println(departments);
//        }

    }
}

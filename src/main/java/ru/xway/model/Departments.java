package ru.xway.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@NamedQueries({
        @NamedQuery(
                name = "Departments.findById",
                query = "select d from Departments d where d.id = :id"
        ),
        @NamedQuery(
                name = "Departments.findAll",
                query = "select d from Departments d"
        ),
        @NamedQuery(
                name = "Departments.delete",
                query = "delete from Departments d where d.id = :id"
        )
})
public class Departments implements Serializable {
    @Id
    @GeneratedValue
    private long id;

    private String name;

    @OneToMany(mappedBy = "departmentId", cascade = CascadeType.ALL)
    private List<Employee> employeesList;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Employee> getEmployeesList() {
        return employeesList;
    }

    public void setEmployeesList(List<Employee> employeesList) {
        this.employeesList = employeesList;
    }

    @Override
    public String toString() {
        return "Departments{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}

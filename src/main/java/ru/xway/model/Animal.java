package ru.xway.model;


public interface Animal {
    String getName();

    int getAge();

    void voice();

    void setName(String name);
}

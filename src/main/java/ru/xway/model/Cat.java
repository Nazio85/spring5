package ru.xway.model;


import org.springframework.lang.NonNull;

public class Cat implements Animal {
    private String name;
    private int age;


    public Cat(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(@NonNull String name) {
        this.name = name;
    }

    @Override
    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void voice() {
        System.out.println("мяууу");
        System.out.println("мяууу");
        System.out.println("мяууу");
        System.out.println("мяууу");
    }
}

//package ru.xway.model.dificult;
//
//import com.ecommpay.host.hap.dao.dblocal.entities.*;
//import com.ecommpay.host.hap.dao.dblocal.entities.account.Account;
//import com.ecommpay.host.hap.dao.dblocal.entities.phone.PhonemapForSearch;
//import lombok.Getter;
//import lombok.Setter;
//import org.hibernate.annotations.Where;
//
//import javax.persistence.*;
//import java.sql.Date;
//import java.util.Set;
//
//@Entity
//@Table(name = "CARD", schema = "CMSSYS")
////@Getter
////@Setter
//public class CardForSearch implements Historical {
//
//    @EmbeddedId
//    private CardIdentity id;
//    private String cardbin;
//    private String cardbody;
//    private String cardcheck;
//
//
//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumns({
//            @JoinColumn(name = "intacccode"),
//            @JoinColumn(name = "dateacccode")
//    })
//    private Account account;
//
//
//
//    @OneToMany
//    @OrderBy("adddate DESC")
//    @JoinColumns({
//            @JoinColumn(name = "intownercode"),
//            @JoinColumn(name = "dateownercode")
//    })
//    @Where(clause = "sintownertype = 9 and sintinfotype = 17")
//    private Set<PhonemapForSearch> phonemaps;
//
//
//
//    @ManyToOne
//    @JoinColumns({
//            @JoinColumn(name = "intcardcode", referencedColumnName = "intcardcode", insertable = false, updatable = false),
//            @JoinColumn(name = "keydate", referencedColumnName = "datecardcode", insertable = false, updatable = false)
//    })
//    @Where(clause = "ownertype = 1")
//    private InCards inCards;
//
//
//    @Override
//    public int getIntKey() {
//        return id.getIntcardcode();
//    }
//
//    @Override
//    public Date getDateKey() {
//        return id.getKeydate();
//    }
//}

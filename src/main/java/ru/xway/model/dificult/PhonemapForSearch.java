package com.ecommpay.host.hap.dao.dblocal.entities.phone;

import com.ecommpay.host.hap.dao.dblocal.entities.card.CardForSearch;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.JoinColumnOrFormula;
import org.hibernate.annotations.JoinColumnsOrFormulas;
import org.hibernate.annotations.JoinFormula;

import javax.persistence.*;

@Entity
@Table(name = "PHONEMAP", schema = "CMSSYS")
@Setter
@Getter
public class PhonemapForSearch {
    @EmbeddedId
    private PhonemapIdentity id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumns({
            @JoinColumn(name = "intphonecode"),
            @JoinColumn(name = "datephonecode")
    })
    private PhoneForSearch phone;


    @ManyToOne
    @JoinColumnsOrFormulas({
            @JoinColumnOrFormula(formula = @JoinFormula(value = "(select i.intcardcode from cmssys.card as i where i.intcardcode = intownercode " +
                    "and i.keydate = dateownercode and sintownertype = 9 and sintinfotype = 17)")),
            @JoinColumnOrFormula(column = @JoinColumn(name = "dateownercode", insertable = false, updatable = false))
    })
    private CardForSearch card;
}

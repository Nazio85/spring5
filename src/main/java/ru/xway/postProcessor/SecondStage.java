package ru.xway.postProcessor;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.ReflectionUtils;
import ru.xway.model.Cat;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

@Component
@EnableScheduling
public class SecondStage implements BeanPostProcessor {

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) {
        Field name = ReflectionUtils.findField(Cat.class, "name");
        ReflectionUtils.makeAccessible(name);


        if (bean instanceof Cat) {
//            System.out.println("--------------------");
//            System.out.println(ReflectionUtils.getField(name, bean));
            Method voice = ReflectionUtils.findMethod(Cat.class, "voice");
//            ReflectionUtils.doWithMethods(Cat.class, voice);

        }
        return null;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        return null;
    }

//    @Scheduled(fixedRate = 2000)
//    public void scheduleTest() {
//        System.out.println("scheduleTest");
//    }
}

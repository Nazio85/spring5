package ru.xway.model;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;
import ru.xway.conf.MainConfig;
import ru.xway.dao.EmployeeRepository;

import javax.swing.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {MainConfig.class})
public class EmployeeTest {
    private Employee employee;

    @Before
    public void init() {
        employee = new Employee() {{
            setFirstName("Aleksandr");
            setSecondName("Khairutdinov");
        }};
    }

    @Test
    public void testSimpleEmployee() {
        assertEquals(employee.getFirstName(), "Aleksandr");
    }

    @Test
    public void testMockito() {
        Employee employeeMock = mock(Employee.class);
        when(employeeMock.getFirstName()).thenReturn("Petr");
        assertEquals(employeeMock.getFirstName(), "Petr");
    }

    @Test(expected = Exception.class)
    public void OutputStreamWriter_rethrows_an_exception_from_OutputStream()
            throws Exception {
        throw new Exception();
    }


}